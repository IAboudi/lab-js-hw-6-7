import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { FamilyService } from '../../services/family.service';


@Component({
  selector: 'lab-js-add-family',
  templateUrl: './add-family.component.html',
  styleUrls: ['./add-family.component.scss']
})
export class AddFamilyComponent implements OnInit {
  public familyForm: FormGroup;
  public get children() {
    return this.familyForm.get('children') as FormArray;
  }
  public constructor(
    private readonly familyService: FamilyService
  ) {

  }
  public ngOnInit(): void {
    this.familyForm = new FormGroup ({
      name: new FormControl('', Validators.required),
      father :  new FormGroup ({
        name : new FormControl('', Validators.required),
        age : new FormControl('', Validators.required)
      }),
      mother :  new FormGroup ({
        name : new FormControl('', Validators.required),
        age : new FormControl('', Validators.required)
      }),
      children : new FormArray([
        new FormGroup({
          name : new FormControl('', Validators.required),
          age : new FormControl('', Validators.required)
        }, Validators.required)
      ])
  }
    )
  }
  public addChild() {
    const child = new FormGroup({
      name: new FormControl('', Validators.required),
      age: new FormControl('', Validators.required)
    });
    (this.familyForm.get('children') as FormArray).push(child);
  }
  public removeChild(index: number) {
    this.children.removeAt(index);
  }
  public submit() {
    if (this.familyForm.valid){
      this.familyService.addFamily$(this.familyForm.value).subscribe()
    }
  }
}
